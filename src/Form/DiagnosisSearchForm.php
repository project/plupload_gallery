<?php

namespace Drupal\med_cases\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Builds the search form for the search block.
 */
class DiagnosisSearchForm extends FormBase {

  /**
   * The search page repository.
   *
   * @var \Drupal\search\SearchPageRepositoryInterface
   */
  //protected $searchPageRepository;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  //protected $configFactory;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  //protected $renderer;

  /**
   * Constructs a new SearchBlockForm.
   *
   * @param \Drupal\search\SearchPageRepositoryInterface $search_page_repository
   *   The search page repository.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Render\RendererInterface
   *   The renderer.
   */
//  public function __construct(SearchPageRepositoryInterface $search_page_repository, ConfigFactoryInterface $config_factory, RendererInterface $renderer) {
//    $this->searchPageRepository = $search_page_repository;
//    $this->configFactory = $config_factory;
//    $this->renderer = $renderer;
//  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'diagnosis_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $type = 'case';

    $form['diagnosis_string'] = array(
      '#type' => 'textfield',
      '#title' => 'Diagnosis Search',
    );
    $form['content_type'] = array(
      '#type' => 'hidden',
      '#value' => $type,
    );
    $form['submit_form'] = array(
      '#value' => t('Enter'),
      '#type' => 'submit',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $diagnosis = $form_state->getValue('diagnosis_string');
    $form_state->setRedirect('view.cases.page_4', ['diagnosis' => $diagnosis]);
  }

}
